package im.gitter.gitter;

public interface PositionClickListener {
    void onClick(int position);
}
