package im.gitter.gitter.utils;

public class MongoUtils {
    public static String idFromUnixSeconds(int unixSeconds) {
        // pads hex val with up to 8 zeros and and adds 16 zeros to the end
        return String.format("%08x0000000000000000", unixSeconds);
    }

    public static int unixSecondsFromId(String id) {
        return Integer.valueOf(id.substring(0, 8), 16);
    }
}
